- `yarn start` or `npm start` to run create-react-app dev mode

- `yarn clean` or `npm run clean` to remove the build/ directory

- `yarn build` or `npm run build` to run `clean` and build the app with create-react-app and run react-snap

This repo does not work. Everything appears to build correctly, and the JS is included in a script tag, but does not seem to run.

Chrome displays an error in the console;

`The resource http://localhost:5000/static/js/1.56cbd7b0.chunk.js was preloaded using link preload but not used within a few seconds from the window's load event. Please make sure it has an appropriate `as` value and it is preloaded intentionally.`

[gitlab.com/GeordieP/snap-issue-parcel](https://gitlab.com/GeordieP/snap-issue-parcel) is the Parcel version of the same app, and JS works as expected there.
